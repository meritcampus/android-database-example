package com.example.deletedatafromdatabase;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DeleteStudentData extends Activity implements OnClickListener {
	EditText editText;
	Button deleteButton;
DataBaseHelper dbHelper;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.delete_data_layout);
		editText = (EditText) findViewById(R.id.id_edittext);
		deleteButton = (Button) findViewById(R.id.delete_button);
		deleteButton.setOnClickListener(this);
		dbHelper = new DataBaseHelper(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.delete_button:
			if(editText.getText().toString().length() >0){
				Student student =new Student();
				student.setRollNumber(Integer.parseInt(editText.getText().toString()));
				int result = dbHelper.deleteStudent(student);
				if(result == 1){
				Toast.makeText(getApplicationContext(), "Deleted student details successfully", Toast.LENGTH_SHORT).show();
				}else{
					Toast.makeText(getApplicationContext(), "No details found", Toast.LENGTH_SHORT).show();
				}
			}
			else{
				Toast.makeText(getApplicationContext(), "Fields should not be empty", Toast.LENGTH_SHORT).show();
			}
			this.finish();
			break;

		default:
			break;
		}
		
	}
}
