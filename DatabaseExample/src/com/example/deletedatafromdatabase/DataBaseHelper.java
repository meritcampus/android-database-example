package com.example.deletedatafromdatabase;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "studentDetails";

	// table name
	private static final String TABLE_STUDENT = "StudentTable";

	// Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_MARKS = "marks";
	private static final String KEY_NAME = "name";
	private static final String KEY_SECTION = "section";
	private static final String KEY_ROLL_NUMBER = "rollnumber";

	public DataBaseHelper(Context context) {
		super(context, TABLE_STUDENT, null, DATABASE_VERSION);

	}

	// create table
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_STUDENT_TABLE = " CREATE TABLE " + TABLE_STUDENT + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_MARKS + " TEXT,"
				+ KEY_NAME + " TEXT," + KEY_SECTION + " TEXT,"
				+ KEY_ROLL_NUMBER + " TEXT" + ")";
		db.execSQL(CREATE_STUDENT_TABLE);
		System.out.println("created database successfully");

	}

	// upgrade table
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_STUDENT);

		// Create tables again
		onCreate(db);

	}

	// Insert student details
	long addStudentDetails(Student student) {
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, student.getName());
		values.put(KEY_MARKS, student.getMarks());
		values.put(KEY_SECTION, student.getSection() + "");
		values.put(KEY_ROLL_NUMBER, student.getRollNumber() + "");
		System.out.println(" inserting ");

		// Inserting Row
		 long insert = database.insert(TABLE_STUDENT, null, values);
		database.close();
		return insert;

	}
	public int updateStudent(Student student) {
	    SQLiteDatabase db = this.getWritableDatabase();
	 
	    ContentValues values = new ContentValues();
	    values.put(KEY_NAME, student.getName());
	    values.put(KEY_MARKS, student.getMarks());
	    values.put(KEY_SECTION, student.getSection()+"");

	    values.put(KEY_ROLL_NUMBER, student.getRollNumber());

	 
	    // updating row
	    return db.update(TABLE_STUDENT, values, KEY_ROLL_NUMBER + " = ?",
	            new String[] { String.valueOf(student.getRollNumber()) });
	}
	// get all student details
	public List<Student> getAllDetails() {
		List<Student> studentList = new ArrayList<Student>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_STUDENT;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Student student = new Student();
				student.setMarks((Integer.parseInt(cursor.getString(1))));
				student.setName(cursor.getString(2));
				student.setSection(cursor.getString(3).charAt(0));
				student.setRollNumber(Integer.parseInt(cursor.getString(4)));
				// Adding student to list
				System.out.println(student.getName() + " ");
				studentList.add(student);
			} while (cursor.moveToNext());
		}

		return studentList;

	}
	// detete student details
	public int deleteStudent(Student student) {
		SQLiteDatabase db = this.getWritableDatabase();

		int delete = db.delete(TABLE_STUDENT, KEY_ROLL_NUMBER + " = ?",
				new String[] { String.valueOf(student.getRollNumber()) });
		db.close();
		return delete;
	}

}
