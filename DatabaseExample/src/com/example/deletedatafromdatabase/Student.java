package com.example.deletedatafromdatabase;

public class Student {
	String name;
	int marks;
	char section;
	 int rollNUmber;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	public char getSection() {
		return section;
	}
	public void setSection(char section) {
		this.section = section;
	}
	public int getRollNumber() {
		return rollNUmber;
	}
	public void setRollNumber(int rollNUmber) {
		this.rollNUmber = rollNUmber;
	}
	

}
