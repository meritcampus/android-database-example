package com.example.deletedatafromdatabase;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class CustomAdapter extends BaseAdapter {
	List<Student> studentList = new ArrayList<Student>();
	Context context;
	int resourceId;

	CustomAdapter(Context context, int id, List<Student> list) {
		this.context = context;
		this.resourceId = id;
		this.studentList = list;

	}

	@Override
	public int getCount() {

		return studentList.size();
	}

	@Override
	public Student getItem(int position) {

		return studentList.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.list_row, null);
		// get id from xml file
		TextView studentName = (TextView)view.findViewById(R.id.student_name);
		TextView marks = (TextView)view.findViewById(R.id.student_marks);
		TextView section = (TextView)view.findViewById(R.id.student_section);
		Button update = (Button)view.findViewById(R.id.update_item_button);
		LinearLayout mainLayout = (LinearLayout)view.findViewById(R.id.main_layout);
		// set data one by one
		studentName.setText("Name :" +studentList.get(position).getName());
		marks.setText("Marks :" +studentList.get(position).getMarks() +"");
		section.setText("Section :" +studentList.get(position).getSection()+"");
		
		
		// onclick for list item
		mainLayout.setOnClickListener(createOnClickListener(position, parent));
		update.setOnClickListener(createOnClickListener(position, parent));
		return view;
	}
	public OnClickListener createOnClickListener(final int position, final ViewGroup parent) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 0);
            }
        };
    }

	

}
