package com.example.deletedatafromdatabase;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class StudentDetails extends Activity implements OnItemClickListener {
	List<Student> studentList;
	ListView listview;
	public static final int REQUEST_CODE = 100; 
	DataBaseHelper dbHelper ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.update_layout);
		listview = (ListView) findViewById(R.id.listView);
		 dbHelper = new DataBaseHelper(getApplicationContext());
		// get all student details
		studentList = dbHelper.getAllDetails();
		// show all student details list
		showAllDetails();
		listview.setOnItemClickListener(this);
	}

	public void showAllDetails() {
		CustomAdapter adapter = new CustomAdapter(getApplicationContext(),
				R.layout.list_row, studentList);
		listview.setAdapter(adapter);
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Student student = studentList.get(position);
		Intent intent = new Intent(getApplicationContext(), UpdateRow.class);

		intent.putExtra("name", student.getName());
		intent.putExtra("marks", student.getMarks());
		intent.putExtra("section", student.getSection());
		intent.putExtra("rollnumber", student.getRollNumber());

		startActivityForResult(intent, 100);

	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			studentList = dbHelper.getAllDetails();
			showAllDetails();
		}
	}
	
}
