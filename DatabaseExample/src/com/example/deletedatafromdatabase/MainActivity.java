package com.example.deletedatafromdatabase;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {
	Button addButton, deleteButton,updateButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		addButton = (Button) findViewById(R.id.add_button);
		deleteButton = (Button) findViewById(R.id.delete_button);
		updateButton =(Button) findViewById(R.id.update_button);
		addButton.setOnClickListener(this);
		deleteButton.setOnClickListener(this);
		updateButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.add_button:
			Intent addIntent = new Intent(getApplicationContext(),AddStudentData.class);
			startActivity(addIntent);
			break;
		case R.id.delete_button:
			Intent deleteIntent = new Intent(getApplicationContext(),DeleteStudentData.class);
			startActivity(deleteIntent);
			break;
		case R.id.update_button:
			Intent updateIntent = new Intent(getApplicationContext(),StudentDetails.class);
			startActivity(updateIntent);
			break;
		}

	}

}
