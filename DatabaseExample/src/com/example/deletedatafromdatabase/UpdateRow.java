package com.example.deletedatafromdatabase;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UpdateRow extends Activity implements OnClickListener{
	String name;int marks; char section;
	EditText firstName, totalMarks, studentSection, rollNum;
	Button btnUpdate;
	DataBaseHelper dbHelper;
	
@Override
protected void onCreate(Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);
	setContentView(R.layout.add_data_layout);
	Bundle bundle = getIntent().getExtras();
	name = bundle.getString("name", "");
	marks= bundle.getInt("marks");
	section = bundle.getChar("section");
	int rollNumber=bundle.getInt("rollnumber");
	firstName = (EditText) findViewById(R.id.name_edittext);
	totalMarks = (EditText) findViewById(R.id.marks_edittext);
	studentSection = (EditText) findViewById(R.id.section_edittext);
	rollNum =(EditText) findViewById(R.id.roll_edittext);
	rollNum.setEnabled(false);
	btnUpdate =(Button) findViewById(R.id.add_button);
	btnUpdate.setOnClickListener(this);
	firstName.setText(name);
	totalMarks.setText(marks+"");
	studentSection.setText(section+"");
	rollNum.setText(rollNumber+"");
	dbHelper=new DataBaseHelper(getApplicationContext());
	
	
}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.add_button:
			if (firstName.getText().toString().length() > 0&& totalMarks.getText().toString().length() > 0&& studentSection.getText().toString().length() > 0&& rollNum.getText().toString().length() > 0) {

				Student student = new Student();
				student.setName(firstName.getText().toString());
				student.setMarks(Integer.parseInt(totalMarks.getText().toString()));
				student.setSection(studentSection.getText().toString().charAt(0));
				student.setRollNumber(Integer.parseInt(rollNum.getText().toString()));
				int update = dbHelper.updateStudent(student);
				if(update == 1){
					Toast.makeText(getApplicationContext(),
							"Student details updated successfully",
							Toast.LENGTH_SHORT).show();
					setResult(RESULT_OK);
					this.finish();
				}else{
					Toast.makeText(getApplicationContext(),
							"Student details not updated ",
							Toast.LENGTH_SHORT).show();
				}
			}
			
			break;

		default:
			break;
		}
		
		
	}

}
